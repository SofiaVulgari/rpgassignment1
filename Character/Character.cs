﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1Backend.Character
{
    public abstract class Character
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttribute PrimaryAttributes { get; set; }
        public PrimaryAttribute TotalPrimaryAttributes { get; set; }
        

        public abstract PrimaryAttribute LevelUp();

        public double CharacterDps { get; set; }

        /// <summary>
        /// Displays data of a character
        /// </summary>
        /// <returns>StringBuilder</returns>
        public string CharacterStatsDisplay()
        {
            StringBuilder StringBuilder = new StringBuilder();

            StringBuilder.Append("Character name: ").Append(Name + "\n");
            StringBuilder.Append("Character Level: ").Append(Level + "\n");
            StringBuilder.Append("Strength: ").Append(TotalPrimaryAttributes.Strength + "\n"); 
            StringBuilder.Append("Dexterity: ").Append(TotalPrimaryAttributes.Dexterity + "\n");
            StringBuilder.Append("Intelligence: ").Append(TotalPrimaryAttributes.Intelligence + "\n");
            StringBuilder.Append("DPS: ").Append(CharacterDps);

            return StringBuilder.ToString();
        }
        
    }
}
