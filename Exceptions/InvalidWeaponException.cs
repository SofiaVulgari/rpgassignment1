﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1Backend.Exceptions
{
    public class InvalidWeaponException : Exception
    {
        /// <summary>
        /// Exception for not correct weapon or not correct level
        /// </summary>
        /// <param name="message"></param>
        public InvalidWeaponException(string message) : base(message)
        {
          
        }
    }
}
