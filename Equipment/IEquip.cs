﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1Backend.Equipment
{
    /// <summary>
    /// Interface to equip items.
    /// </summary>
    public interface IEquip
    {
        String EquipWeapon(ItemSlot itemSlot, Weapon weapon);

        String EquipArmor(ItemSlot itemSlot, Armor armor);
    }
}
