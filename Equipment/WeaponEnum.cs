﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1Backend.Equipment
{
    /// <summary>
    /// Enum for the different weapons
    /// </summary>
    public enum WeaponEnum
    {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }
}
