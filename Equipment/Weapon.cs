﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1Backend.Equipment
{
    public class Weapon : Item
    {
        public double BaseDamage { get; set; }
        public double AttacksPerSec { get; set; }
        public double WeaponDps { get; set; }

        public WeaponEnum WeaponEnum { get; set; }

        public Weapon()
        {

        }

        /// <summary>
        /// Weapon is created with the weapon enum, and its name, requiredLevelel and itemSlot taken from parent, and base damage and attacks per second to calculate the weapon's DPS. 
        /// </summary>
        /// <param name="name">string</param>
        /// <param name="weaponEnum">WeaponEnum</param>
        /// <param name="requiredLevel">int</param>
        /// <param name="itemSlot">ItemSlot</param>
        /// <param name="baseDamage">double</param>
        /// <param name="attacksPerSec">double</param>
        public Weapon(String name, WeaponEnum weaponEnum, int requiredLevel, ItemSlot itemSlot, double baseDamage, double attacksPerSec) : base(name, requiredLevel, itemSlot)
        {
            WeaponEnum = weaponEnum;
            AttacksPerSec = attacksPerSec;
            BaseDamage = baseDamage;
            WeaponDps = baseDamage * attacksPerSec;
        }
    }
}
